import express from "express";
import cors from "cors";
import fetch from "isomorphic-fetch";
import _ from "lodash";

const app = express();
app.use(cors());

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async(res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });

app.get('/*', (req, res) => {
  var path = _.tail(_.split(_.replace(req.url, /[\.,\[,\]]/, '+'), '/')).join(".");

  if (path && path !== 'volumes') {

    const result = _.get(pc, path);

    if (!_.isUndefined(result)) {
      res.json(result);
    } else {
      res.status(404).send('Not found');
    }

  } else if (path === 'volumes') {
    const obj = _.mapValues(_.reduce(pc.hdd, function (result, value, key) {

      if (result[value['volume']]) {
        result[value['volume']] = result[value['volume']] + value['size'];
      } else {
        result[value['volume']] = value['size'];
      }
      return result;
    }, {}), function (o) {
      console.log(o);
      return {}[o.key] = o + "B";
    });

    res.json(obj);
  } else {
    res.json(pc);
  }

});

app.get('/sum', (req, res) => {
  const a = parseInt(req.query.a) | 0;
  const b = parseInt(req.query.b) | 0;
  const sum = a + b;

  res.send(sum.toString());
});

const regExp = /(^[^ \[\]\!\"\#\$\%\&\(\)\*\+\,\.\/\:\;\<\=\>\?\@\\\_\{\|\}\~\-\d]+\s+)?([^ \[\]\!\"\#\$\%\&\(\)\*\+\,\.\/\:\;\<\=\>\?\@\\\_\{\|\}\~\-\d]+\s+)?([^ \[\]\!\"\#\$\%\&\(\)\*\+\,\.\/\:\;\<\=\>\?\@\\\_\{\|\}\~\-\d]+$)/;

app.get('/initialize', (req, res) => {
  const fullnameData = _.trim(req.query.fullname);
  const match = fullnameData.match(regExp);

  if (match && match.index == 0) {
    const nameArray = _.takeRight(match, 3).map(item => _.capitalize(item));
    const filteredNameArray = _.filter(nameArray, elem => !!elem);
    const surname = _.last(nameArray);
    const initials = _.initial(filteredNameArray, 2).map(elem => ` ${_.head(elem)}.`).join('');
    const result = surname + initials;
    res.send(result);
  } else {
    res.send('Invalid fullname');
  }

});

app.get('/canonize', (req, res) => {
  const usernameData = req.query.username;

  const regEx = /(https?:)?(\/\/)?(www.)?([\w\d\-]+.[\w\d\-]{2,}\/)?(@)?([\w\d.]+)(\?.+)?/;
  const username = usernameData.match(regEx);
  console.log(username);
  if (username && username[6]) {
    return res.send(`@${username[6]}`);
  } else {
    return res.send('Invalid username');
  }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
